package redis

import (
	"github.com/gomodule/redigo/redis"
	"github.com/sirupsen/logrus"
	"time"
)

var pool *redis.Pool

type Config struct {
	Server      string
	Password    string
	MaxIdle     int
	IdleTimeout time.Duration
	MaxActive   int
	Wait        bool
}

func InitPool(conf Config) {
	pool = &redis.Pool{
		MaxIdle:     conf.MaxIdle,
		IdleTimeout: conf.IdleTimeout,
		MaxActive:   conf.MaxActive,
		Wait:        conf.Wait,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", conf.Server)
			if err != nil {
				logrus.Errorln("Redis: Dial failed", err)
				return nil, err
			}
			if _, err := c.Do("AUTH", conf.Password); err != nil && conf.Password != "" {
				logrus.Errorln("Redis: AUTH failed", err)
				_ = c.Close()
				return nil, err
			}
			return c, err
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			if time.Since(t) < time.Minute {
				return nil
			}
			_, err := c.Do("PING")
			return err
		},
	}
}

func Run(command Command, result interface{}) error {
	conn := pool.Get()
	defer func() {
		err := conn.Close()
		if err != nil {
			logrus.Errorln(err)
		}
	}()

	reply, err := conn.Do(command.Command(), command.Params()...)
	if err != nil {
		return err
	}

	return command.Result(reply, result)
}
