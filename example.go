package redis

import (
	"fmt"
	"newtb/TBConstants"
	"time"
)

func main() {
	TBConstants.Init("env.json")
	config := Config{
		Server:      TBConstants.CUR_ENV.REDIS_SERVER,
		Password:    TBConstants.CUR_ENV.REDIS_PASSWORD,
		MaxIdle:     3,
		IdleTimeout: 240 * time.Second,
	}
	InitPool(config)

	set := Set{Key: "key1", Value: "value1", EX: 100, NX: true}
	result, err := set.Run()
	if err != nil {
		panic(err)
	}
	fmt.Println(result)
}
