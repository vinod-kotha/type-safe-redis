package redis

import (
	"fmt"
)

var (
	NotValidType = fmt.Errorf("not a valid return type")
)

type Command interface {
	Command() string
	Params() []interface{}
	Result(repy interface{}, value interface{}) error
}
