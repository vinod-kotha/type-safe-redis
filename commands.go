package redis

import "github.com/gomodule/redigo/redis"

type Set struct {
	Key   string
	Value string
	EX    int64
	PX    int64
	NX    bool
	XX    bool
}

func (s *Set) Command() string {
	return "SET"
}

func (s *Set) Params() (params []interface{}) {
	params = append(params, s.Key, s.Value)

	if s.EX != 0 {
		params = append(params, "EX", s.EX)
	} else if s.PX != 0 {
		params = append(params, "PX", s.PX)
	}

	if s.NX {
		params = append(params, "NX")
	} else if s.XX {
		params = append(params, "XX")
	}

	return
}

func (s *Set) Result(repy interface{}, result interface{}) error {
	val, ok := result.(*string)
	if !ok {
		return NotValidType
	}
	value, err := redis.String(repy, nil)
	if err != nil {
		return err
	}
	*val = value
	return nil
}

func (s *Set) Run() (string, error) {
	var result string
	err := Run(s, &result)
	return result, err
}
